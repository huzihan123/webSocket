package com.huzihan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestWs {

    public static void main(String[] args) {
        SpringApplication.run(TestWs.class);
    }
}
